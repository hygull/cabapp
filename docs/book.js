function sendBookingInfo(){
	try{
				if (isCookieSet() === false) {
					alert("You need to login to book")
					location.href = "/v1/login-new/"
					return false
				}
				// document.getElementById('gif-image').style.display = "block";
				console.log("Preparing to send booking details to backend server")
				// alert("Hello")

				var from = document.forms["bookingForm"]["from"].value;
				// alert(from)
				var to = document.forms["bookingForm"]["to"].value;

				var dot = document.forms["bookingForm"]["dot"].value;

				var dor = document.forms["bookingForm"]["dor"].value;

				var mem = document.getElementById("select");
				var members = mem.options[mem.selectedIndex].value

				var hour = document.getElementById("select_hour");
				var hours = hour.options[hour.selectedIndex].value

				var minute = document.getElementById("select_minute");
				var minutes = minute.options[minute.selectedIndex].value

				var period = document.getElementById("select_period");
				var periods = period.options[period.selectedIndex].value

				if (from === "" || to === "" || dot === "" || dor === "" || mem === "" || hour === "" || minute === "" || period === ""){
					alert("All the fields are required")
					return false 
				}

				var dot_arr = dot.split("/")
				console.log(dot_arr)

				var dor_arr = dor.split("/")
				console.log(dor_arr)

				d1 = new Date(dot_arr[2], dot_arr[0], dot_arr[1])
				d2 = new Date(dor_arr[2], dor_arr[0], dor_arr[1])

				console.log(d1)
				console.log(d2)
				console.log(d1<=d2)

				if(!(d1<=d2)){
					alert("Date of travel & return should be proper(D.O.T <= D.O.R)")
					return false
				}
				
	} catch(err){
		alert("Something unexpected occured" + err.message)
		return false
	}

	var contact = localStorage.getItem("contact")
	// var found =false
	// try{
	// 	contact = localStorage.getItem("contact")
	// 	found =true
	// } catch(err) {
	// 	console.log("Error occured")
	// 	alert("Not found")
	// }

	// if(found){
	// 	console.log("Found")
	// } else {
	// 	console.log("Not found")
	// 	alert("contact not found, need to set")
	// 	return false
	// }
    // fetch()
    // .then(function(response) {
    //     if(response.status == 200) return response.json();
    //     else throw new Error('Something went wrong on api server!');
    // })
    // .then(function(response) {
    //     console.debug(response);
    //     // ...
    // })
    // .catch(function(error) {
    //     console.error(error);
    // });

 //    var myRequest = new Request('http://127.0.0.1:8000/v1/login/', {method: 'POST', body: '{"mob":"7353787704", "app_key":"a9316ba8085e74e780444c0d598d7bbe"}'});
	// console.log(myRequest)

fetch('/v1/booking/book-now/', {
		  method: 'post',
		  headers: {
		    'Accept': 'application/json, text/plain, */*',
		    'Content-Type': 'application/x-www-form-urlencoded',
		    "Access-Control-Allow-Origin": "*",

		  },
		  body: JSON.stringify({from: from, to: to, dot: dot, dor: dor, members: members, minutes: minutes, hours: hours, periods:periods, contact:contact})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			console.log(typeof response)
			console.log(response.message)
			
			console.log("Done")

			if (response.status == 200){
				console.log("Status OK")
				// var s = "app_key="+appKey+"; user_id="+response.user_id
				// console.log("s = "+s)
				// document.cookie = stringify
				alert(response.message)
				// var data = {"app_key": appKey, "user_id": response.user_id}
				// document.cookie = "myCookieData="+JSON.stringify(data)
				// console.log(document.cookie)
				// console	.log("Nice work")
				// window.location.href = "http://127.0.0.1:8000/otp/"

				if (localStorage) {
					//Clear all keys
					
					alert("Your browser supports localStorage");

					localStorage["members"] = members;

					localStorage["hours"] = hours;

					localStorage["minutes"] = minutes;

					localStorage["periods"] = periods; 

					localStorage["booking_id"] = response.booking_id

					localStorage["dot"] = dot.split("/").join("-")

					localStorage["dor"] = dor.split("/").join("-")
					alert("SET")

				} else {
					alert("Your browser don't support localStorage, use latest Google Chrome");
				}

				console.log("Redirect link: "+"/v1/booking/vehicles-list/"+members+"/"+dot_arr.join("-")+"/"+dor_arr.join("-")+"/")
				// document.getElementById("bookingForm").reset();
				location.href = "/v1/booking/vehicles-list/"+members+"/"+dot_arr.join("-")+"/"+dor_arr.join("-")+"/"

				return 	true
			} else {
				console.log("400/500", response.message)
				alert(response.message)
				return false
			}
		})
		.catch(function(){
			console.log("Error ocurred, try again.")
			// console.log("Error: ", response.message)
			// alert("Error: ", response.message)
		});
	return false
}


