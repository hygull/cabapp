# #1
Don't place the following line in app's urls.py.

It should only be on project's urls.py.

```
urlpatterns = format_suffix_patterns(urlpatterns)
```

Other wise

```
django.core.exceptions.ImproperlyConfigured: "^login\.(?P<format>[a-z0-9]+)/?\.(?P<format>[a-z0-9]+)/?$" is not a valid regular expression: redefinition of group name u'format' as group 2; was group 1
```