# #1

```
STATIC_URL = '/static/'

MEDIA_URL = "/media/"

MEDIA_ROOT = os.path.join(BASE_DIR, "static", "media")
```

is sufficient for media upload.

# #2

You don't need to create folders like static,media_dir static_dir etc.

When you will upload the file, automatically it will be created by the Django.

```
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static", "static_dir")
MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "static", "media_dir")

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static")
]
```

# #3	
```
(venv) MacBook-Pro:Login admin$ ls
__init__.py	admin.pyc	models.py	services.pyc	urls.pyc	views.py
__init__.pyc	apps.py		models.pyc	tests.py	validators.py	views.pyc
admin.py	migrations	services.py	urls.py		validators.pyc
(venv) MacBook-Pro:Login admin$ python ../manage.py shell
Python 2.7.13 (v2.7.13:a06454b1afa1, Dec 17 2016, 12:39:47) 
[GCC 4.2.1 (Apple Inc. build 5666) (dot 3)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from .models import User
Traceback (most recent call last):
  File "<console>", line 1, in <module>
ImportError: No module named models
>>> exit()
(venv) MacBook-Pro:Login admin$ cd ..
(venv) MacBook-Pro:src admin$ ls
CabProj		Login		db.sqlite3	manage.py	static
(venv) MacBook-Pro:src admin$ python manage.py shell
Python 2.7.13 (v2.7.13:a06454b1afa1, Dec 17 2016, 12:39:47) 
[GCC 4.2.1 (Apple Inc. build 5666) (dot 3)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from Login.models import User
>>> user = User.objects.get(id=1)
>>> 
>>> 
>>> user.id
1
>>> user.contact
7353787704
>>> type(user.contact)
<type 'int'>
>>> user = User.objects.get(contact=8861815816)
>>> user.id
2
>>> user = User.objects.get(contact="8861815816")
>>> user = User.objects.get(contact="7353787704")
>>> user.id
1
>>> 
```