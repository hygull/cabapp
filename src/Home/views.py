# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from .models import VisitorDetail 


def visitors_details(request):
	vistors = VisitorDetail.objects.all()
	return render(request, "visitors_details.html", {"visitors": vistors})


# x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
#    if x_forwarded_for:
#        ip = x_forwarded_for.split(',')[-1].strip()
#    else:
#        ip = request.META.get('REMOTE_ADDR')
#    return ip

def my_custom_page_not_found_view(request):
	response = render_to_response("400.html", context_instance=RequestContext(request))
	response.status_code = 400
	return response


def not_found(request):
	return render(request, "page_not_found.html", {})