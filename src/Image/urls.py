from django.conf.urls import include, url

from Image import views

try: 
	urlpatterns = [
	    # url(r"^stores/$", views.get_store_images, name="get_store_images"),
	    url(r"^(?P<slug>[-\w]+)/$", views.get_images, name="get_images"),
	    # url(r"^get-token/$", views.get_token, name="get_token"),
	    # url(r"^users/$", views.UsersAPIView.as_view(), name="users"),
	    # url(r"^users/(?P<id>[0-9]+)/$", views.UserAPIView.as_view(), name="user"),
	    # url(r"^update-profile/(?P<id>[0-9]+)/$", views.update_profile, name="update_profile"),
	]
except:
	print "Error ocurred" 
