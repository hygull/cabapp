"""SureCabsProj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import viewsets,routers
from Login import views 
from Home import views as home_view 
from django.conf.urls import handler404

handler404 = 'Home.views.my_custom_page_not_found_view'

urlpatterns = [
    # ORIGINAL URLS
    url(r"^comments/", include("Comment.urls")),
    url(r"^faqs/", include("Faq.urls")),
    url(r"^contact/", include("Contact.urls")),
    url(r"^offers/", include("Offer.urls")),
    url(r"^register/", include("Register.urls")),
    url(r"^trips/", include("Trip.urls")),
    url(r"^holiday-packages/", include("Trip.urls")),

    # TEST URLS
    url(r'^admin/', include(admin.site.urls)),
    url(r"^v1/", include("Login.urls")),
    url(r"^v1/images/", include("Image.urls")),
    url(r"^home-old$", views.home, name="home"),
    url(r"^home/$", views.home2, name="home2"),
    # url(r"^booking/$", views.booking, name="booking"),
    url(r"^otp/$", views.otp, name="otp"),
    url(r"^book/$", views.book, name="book"),
    url(r"^v1/booking/", include("Book.urls")),

    # SCRATCH DEVELOPMENT
    # url(r"^contact/", views.contact, name="contact"),
    url(r"^about/", views.about, name="about"),
    url(r"^book-new/", views.book_new, name="book-new"),
    url(r"^$", views.home_new, name="home-new"),

    # url(r"^comments/", views.comments, name="comments"),
    # url(r"^faq/", views.faq, name="faq"),
    # url(r"^offers/", views.offers, name="offers"),

    url(r"^trial/", views.trial, name="trial"),
    url(r"^home-visitors/", home_view.visitors_details, name="home-visitors"),
    url(r"^.*$", home_view.not_found, name="not-found"),
] 

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns)

handler404 = 'Home.views.my_custom_page_not_found_view'
# Auth Endpoint(login/logout)
# url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),