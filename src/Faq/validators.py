from django.core.exceptions import ValidationError

def validate_question(question):
	if len(question.strip()) < 10:
		raise ValidationError("Comment should have atleast 10 characters")


