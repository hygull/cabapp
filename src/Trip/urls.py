from django.conf.urls import  url
import views

try: 
	urlpatterns = [
	    url(r"^(?P<slug>[-\w]+)/$", views.trips, name="trips"),
	]
except:
	print "Error Ocurred in Trip/urls.py"
