# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import BookingDetail, Vehicle, OurVehicle, BookingNew, BookingDetailStore

admin.site.register(BookingDetail)

admin.site.register(Vehicle)

admin.site.register(OurVehicle)

admin.site.register(BookingNew)

admin.site.register(BookingDetailStore)
