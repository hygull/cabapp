# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import validators
from Register.models import User

class Vehicle(models.Model):
	CATEGORIES = (
		("cab/taxi", "CAB/TAXI"),
		("enova", "ENOVA"),
		("tempo-traveller", "TEMPO TRAVELLER"),
		("mini-bus", "MINI BUS"),
		("bus", "BUS")
	)

	name = models.CharField(max_length=100, null=False, blank=False)
	image = models.ImageField(default="http://www.meadowsfield.com/wp-content/uploads/2013/11/taxi-3.png", max_length=300, validators=[validators.validate_image] )
	category = models.CharField(max_length=20, choices=CATEGORIES, default="cab")
	price = models.FloatField(null=False, blank=False)
	discount = models.PositiveIntegerField(default=0, null=False, blank=True)
	description = models.TextField(null=False, blank=False)
	items_count = models.PositiveIntegerField(default=0 ,null=False, blank=True)


	def __str__(self):
		msg = "VEHICLE(%s) %s, %s, Rs. %.2f/Hour"%(self.category, self.id,self.name, self.price)
		
		if self.items_count == 0:
			return msg + " 	| Unavailable"
		else:
			return msg + " 	| Available" 



class OurVehicle(models.Model):
	CATEGORIES = (
		("cab/taxi", "CAB/TAXI"),
		("innova", "INNOVA"),
		("tempo-traveller", "TEMPO TRAVELLER"),
		("mini-bus", "MINI BUS"),
		("bus", "BUS")
	)

	name = models.CharField(max_length=100, null=False, blank=False)
	image = models.ImageField(default="http://www.meadowsfield.com/wp-content/uploads/2013/11/taxi-3.png", max_length=300, validators=[validators.validate_image] )
	category = models.CharField(max_length=20, choices=CATEGORIES, default="cab")
	# price = models.FloatField(null=False, blank=False)
	discount = models.PositiveIntegerField(default=0, null=False, blank=True)
	per_km_cost_for_ac = models.FloatField(default=10)
	per_km_cost_for_non_ac = models.FloatField(default=10)
	minimum_km_per_day = models.PositiveIntegerField(default=300)
	# round_trip = models.PositiveIntegerField(default=80)
	max_passengers = models.PositiveIntegerField(default=1) 
	ac_and_non_ac_facility = models.BooleanField(default=False)
	items_count = models.PositiveIntegerField(default=0 ,null=False, blank=True)
	driver_bata = models.PositiveIntegerField(default=300)

	def __str__(self):
		msg = "VEHICLE(%s) %s, %s"%(self.category, self.id,self.name)
		
		if self.items_count == 0:
			return msg + " 	| Unavailable"
		else:
			return msg + " 	| Available" 

class BookingDetail(models.Model):
	booking_id = models.CharField(max_length=200, null=False, blank=False, unique=True)
	customer = models.ForeignKey(User, on_delete=models.CASCADE,default="1")
	vehicles = models.ManyToManyField(OurVehicle)
	source = models.CharField(max_length=500, null=False, blank=False)
	destination = models.CharField(max_length=500, null=False, blank=False)
	booked_on = models.DateTimeField(auto_now_add=True,auto_now=False)
	edited_on = models.DateTimeField(auto_now_add=False, auto_now=True)
	date_of_travel = models.DateField(blank=False, null=False)
	date_of_return = models.DateField(blank=False, null=False)
	pick_up_time = models.TimeField(blank=False, null=False)
	is_paid = models.BooleanField(default=False, blank=True, null=False)

	def __str__(self):
		return str(self.booked_on)[:19]+" | Booking details - By User Id "+str(self.customer.id) +" With Mobile Number " + str(self.customer.contact)

class BookingNew(models.Model):
	customer = models.ForeignKey(User, on_delete=models.CASCADE)
	booking_id = models.CharField(max_length=200, null=False, blank=False, unique=True)
	vehicles = models.ForeignKey(OurVehicle,on_delete=models.CASCADE)
	source = models.CharField(max_length=500, null=False, blank=False)
	destination = models.CharField(max_length=500, null=False, blank=False)
	booked_on = models.DateTimeField(auto_now_add=True,auto_now=False)
	edited_on = models.DateTimeField(auto_now_add=False, auto_now=True)
	date_of_travel = models.DateField(blank=False, null=False)
	date_of_return = models.DateField(blank=False, null=False)
	pick_up_time = models.TimeField(blank=False, null=False)
	is_paid = models.BooleanField(default=False, blank=True, null=False)

	def __str__(self):
		return str(self.booked_on)[:19]+" | Booking details - By User Id "+str(self.customer.id) +" With Mobile Number " + str(self.customer.contact)


class BookingDetailStore(models.Model):
	booking_id = models.CharField(max_length=100, default="") #done
	fname = models.CharField(max_length=100, default="") #done
	email = models.CharField(max_length=100, default="") #done
	contact = models.CharField(max_length=100, default="") #done
	vehicle = models.CharField(max_length=100, default="") 
	minimum_km_per_day = models.CharField(max_length=50, default="") #done
	driver_bata =  models.CharField(max_length=50, default="") #done
	# pick_up_location =  models.CharField(max_length=300) #done
	
	frm =  models.CharField(max_length=300, default="") #done
	to =  models.CharField(max_length=300, default="") #done
	dot =  models.CharField(max_length=20, default="") #done
	dor =  models.CharField(max_length=20, default="") #done
	pickup_time = models.CharField(max_length=10, default="") #done
	total_days =  models.CharField(max_length=30, default="") #done
	cost1 =  models.CharField(max_length=30, default="") #done
	cost2 = models.CharField(max_length=30, default="") #done
	# total_bata =  models.CharField(max_length=100) #done
	total_cost =  models.CharField(max_length=100, default="") #done		
	vehicle_image_url = models.CharField(max_length=500, default="") #done

	def __str__(self):
		return "NEW BOOKING DETAIL (%s) - %s"%(self.booking_id, self.vehicle)
		