from django.conf.urls import  url
import views

try: 
	urlpatterns = [
	    url(r"^$", views.contact, name="contact"),
	    url(r"^submit/$", views.send_contact_message, name="send-contact-message"),
	]
except:
	print "Error Ocurred in Contact/urls.py"
