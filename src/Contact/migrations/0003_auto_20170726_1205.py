# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-26 12:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Contact', '0002_auto_20170726_1204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='email',
            field=models.EmailField(max_length=10),
        ),
    ]
