from django.core.exceptions import ValidationError
import re

def validate_contact(contact):
	if not re.match(r"^([789])([0-9]{9})$", contact.strip()):
		raise ValidationError("Contact should contain exactly 10 digits and start with 7/8/9")
