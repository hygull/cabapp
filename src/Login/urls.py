from django.conf.urls import include, url
from django.contrib import admin

from Login import views

try: 
	urlpatterns = [
		url(r"^logout/$", views.logout, name="logout"),
		url(r"^login-new/$", views.login_new, name="login-new"),
		url(r"^login/submit/$", views.login, name="login"),
		url(r"^terms-and-conditions/$", views.terms_and_conditions, name="terms-and-conditions"),
	    url(r"^login/$", views.send_otp, name="send_otp"),
	    url(r"^get-token/$", views.get_token, name="get_token"),
	    url(r"^users/$", views.UsersAPIView.as_view(), name="users"),
	    url(r"^users/(?P<id>[0-9]+)/$", views.UserAPIView.as_view(), name="user"),
	    url(r"^update-profile/(?P<id>[0-9]+)/$", views.update_profile, name="update_profile"),
	]
except:
	print "Error Ocurred in Login/urls.py"
# urlpatterns = format_suffix_patterns(urlpatterns)

# Auth Endpoint(login/logout)
# url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),