import requests
from rest_framework.renderers import JSONRenderer
import random
from django.utils import timezone 
import datetime
import jwt
from rest_framework.response import Response


# A function that generates jwt    
def create_jwt(user, app_key):
    payload = {}
    payload["user_id"] = user.id
    payload["contact"] = user.contact
    payload["exp"] = datetime.datetime.utcnow() + datetime.timedelta(minutes=2)

    print "Created payload as ", payload

    jwt_token = jwt.encode(payload, app_key) #algorithm=HS256, typ=JWT, signed with app_key

    print "Created JWT is : %s"%jwt_token

    return jwt_token


# A function that sends sms to mobile
def sms_sender(number, otp):
    print "Got ", number, type(number) ," and ", otp, type(otp)
    smsapi = "http://smsgateway.me/api/v3/messages/send"

    json_data = {
        "email": "offlinetrend@gmail.com",
        "password": "OfflineTrend321",
        "device": "50691",
        "number": number,
        "message": "YOUR OTP IS %s" % (otp)
    }

    print "Sending message to the related device"

    response = requests.post(smsapi, json_data)
    print response
    response_data_str = response.content    #<type 'str'>
    print response_data_str, type(response_data_str)

# A function that sends sms to the related mobile number
def sms(user, mobile_number, data, old_otp=0):
    otp = old_otp
    if otp == 0:
        otp = random.randrange(100000, 999999)
        # Save otp in database
        user.otp = otp
        print "Type test", type(otp), type(user.otp)
        user.otp_created_at = timezone.now() #datetime.datetime.utcnow()
        user.save()

        print "New otp created at ",user.otp_created_at
    
    print "Calling SmsSender with", mobile_number, otp
    sms_sender(mobile_number, otp)

    data.update({
        "status": 200,
        "is_active": True,
        "message": "6 digits otp "+str(otp)+" successfully sent to %s" % (mobile_number),
        "user_id": user.id
    })
                
    print "Updated otp %d to database for user with id %d"%(otp, user.id)

# A function that checks the validity of OTP
def get_otp_creation_time(user):
    otp_created_at = user.otp_created_at
    otp = user.otp

    print "Last otp was: ", otp
    print "And that was created at ", otp_created_at

    current_time = timezone.now()
    diff = current_time - otp_created_at

    print "Current time: ", current_time
    print "Difference(", current_time,",",otp_created_at,"): ", diff

    seconds = diff.total_seconds() 
    print "Difference in seconds: ", seconds
    minutes = seconds/60
    print "Difference in minutes: ", minutes    

    return minutes


# A function that extracts JWT from Header
def extract_jwt_from_header(request):
    # print request.META
    authorization = request.META.get("HTTP_AUTHORIZATION")

    if not authorization: #jwt will be None(if it does not exist in the request header)
        return (None, Response({"status":400, "message": "JWT is required"}, status=400) )
    else:
        authorization = authorization.strip()

    jwt_str_arr = authorization.split()

    if len(jwt_str_arr) != 2 or jwt_str_arr[0] != "Bearer":
        print "Authorization header should have exactly 2 parts like 'Bearer <jwt_token>'"
        return (None, Response({"status": 400, "message": "Authorization header should have exactly 2 parts like 'Bearer <jwt_token>'"}, status=400))

    jwt_str = jwt_str_arr[1]

    print "Got JWT as ", jwt_str
    return jwt_str, None


def user_id_is_valid(user_id):
    print "Request for profile update for user id ", user_id, type(user_id)
    uid = 0
    
    try:
        uid = long(user_id)
    except:
        print "User Id is not valid"
        return None, Response({"status": 400, "message": "user id should be an integer"}, status=400)
    
    if uid < 0:
        return None, Response({"status": 400, "message": "user id should be > 0"}, status=400)

    return uid, None
