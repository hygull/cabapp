from rest_framework import serializers
from .models import User

class UserSerializer(serializers.HyperlinkedModelSerializer):
	""" A serializer class of user model """
	class Meta:
		model = User
		fields = ("id", "fname", "lname", "contact", "email", "profile_pic", "is_active", "address")
							

 