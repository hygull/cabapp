# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.decorators import api_view
import json
from Register.models import User
import requests
from django.core.mail import send_mail
import hashlib
from django.shortcuts import redirect
import conf 

system_data = {}
data = {}

@csrf_exempt
def register(request):
	return render(request, "register.html", {})

def verify_captcha_and_send_mail():
	secret = "6Lf0RCwUAAAAACAMzsI5X_fuMlrUwAmIC8np_yR5"
	remoteip = system_data["ip"]
	# response = data["g_recatcha_response"]
	# endpoint =  "https://www.google.com/recaptcha/api/siteverify"
	# print "Verifying G-REACAPTCHA, Sending data to google server"
	# response2 = requests.post(endpoint, data={"secret": secret, "remoteip": remoteip, "response": response})
	# print "RE-CAPTCHA validation Response code => ", response2.status_code
	# print response2.text

	# if data["fname"] == "":
	# 	data["fname"] = "-"

	# if data["password"] == "":
	# 	data["password"] = "-"

	# Send confirmation link
	send_mail(
		    'CONTACT MAIL FROM '+data["fname"].strip(),	# header
		    "",	# message
		    'surecabstoursandtravels@gmail.com',
		    [ data["email"] ],
		    fail_silently=True,
		    html_message="<table style='border-collapse:collapse; '>"+
		    "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn1.iconfinder.com/data/icons/business-finance-vol-10-2/512/21-24.png'/></td>"+
		    	"<td style='padding:5px'><b>Fullname</b></td>"+
		    	"<td style='padding:5px'>"+data["fname"]+"</td>"
		    "</tr>"+
		    "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn1.iconfinder.com/data/icons/mail-contact-and-subscription/249/mail-4-r-24.png'/></td>"+
		    	"<td style='padding:5px'><b>Email</b></td>"+
		    	"<td style='padding:5px'>"+data["email"]+"</td>"
		    "</tr>"+
		    "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn4.iconfinder.com/data/icons/medical-flat-outline-2/614/485_-_Chat-24.png'/></td>"+
		    	"<td style='padding:5px'><b>Contact</b></td>"+
		    	"<td style='padding:5px'>"+data["contact"]+"</td>"
		    "</tr>"+
		     "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn0.iconfinder.com/data/icons/twitter-ui-flat/48/Twitter_UI-18-32.png'/></td>"+
		    	"<td style='padding:5px'><b>Password</b></td>"+
		    	"<td style='padding:5px'>"+data["password"]+"</td>"
		    "</tr>"+
		    "</table><br><br>"+
		    # "&nbsp;&nbsp;&nbsp;<a href='http://surecabs.in/register/confirm-account/"+
		    "&nbsp;&nbsp;&nbsp;<a href='https://surecabs.in/register/confirm-account/"+
		    conf.EMAIL_TEXT +"/"+
			hashlib.md5(data["email"]).hexdigest()+"/"+ data["email"]+"/'>"+"Click Here"+ "</a> to confirm your account"
	)
	print "Successfully sent the mail to new USER"

@api_view(["POST"])
def store_registration_details(request, format=None):
	global data
	global system_data
	print request.POST
	print request.POST.dict()
	print request.POST.dict().keys()
	print request.POST.dict().keys()[0]
	print "Register Data: ", data.update(json.loads(request.POST.dict().keys()[0]))	
	print data
	
	response = requests.get("https://jsonip.com/")
	print "STATUS CODE while fetching current systems IP ", response.status_code
	system_data = json.loads(response.text)
	print "Your IP is ", system_data["ip"]

	try:
		try:
			user = User.objects.get(contact=data["contact"])
			print "Got user ", user
			return Response({"message": "You are already registered", "status": 700}, status=200)
		except:
			pass

		print "Created a new registration details"
		user = User(fname=data["fname"], contact=data["contact"], email=data["email"], password=data["password"])
		user.save()
		import threading 
			
		threading.Thread(target=verify_captcha_and_send_mail, args=[]).start()
		
		return Response({"message": "You successfully registered with us", "status": 200})
	except Exception as e:
		print e,
		return Response({"message": "Something unexpected happened on server side", "status": 200})
	

def confirm_account(request, email_md5, email):
	print email_md5, email
	if email_md5 != hashlib.md5(email.strip()).hexdigest():
		return redirect("/register/error/")
	return redirect("/register/account-confirmed/"+email.strip()+"/"+ conf.EMAIL_TEXT + "/"+email_md5.strip())

def account_confirmed(request, email, email_md5):
	print "Confirming",email_md5, email
	if email_md5 != hashlib.md5(email.strip()).hexdigest():
		return redirect("/register/error/")
	return render(request, "account_confirmed.html", {"email": email})

def error(request):
	message = "It seems like someone is trying to perform unwanted actions"
	return render(request, "error.html", {"message": message})

